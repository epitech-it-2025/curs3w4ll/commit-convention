- [Conventions](#conventions)
    - [Branch convention](#branch-naming-convention)
    - [Commit convention](#commit-convention)

## Conventions

When contributing to this project, you need to follow a set of rules.

This rules are simple and here to help you keep being organized and facilitate the communication with the team.

First of all, when you want to do something on the project, you should watch the project issues.
You can take any issue that is not already assigned to someone.  
When you want to start working on an issue, you should assign it to you.

### Branch naming convention

When you want to start working on an issue and start coding, you must create a new branch.

Most of the newly created branches are from the dev branch.

When you create a new branch, you can't just named it `mybranch` or `branchToDoThat`.  
You **need** to follow the following convention.

Branches names should look like this
`<issue-number>_<parent-branch>_<issue-summary>`

#### `issue-number`

This is the id of the issue this branch is created to work on.

#### `parent-branch`

The name of the branch this branch is created from (and often will be merged on).

#### `issue-summary`

Most of the time is the title of the issue `-` separated.  
Can also being completed with a short issue description.

#### Example

Branch to do issue #13 and that is from `dev` branch to create a clock.

`13_dev_Create-clock`

### Commit convention

To help the team easily keep an eye on everyone advancement, commits must be clear and detailed.

Commits should look like this

`<type>[(scope)]: <description> [#<issue-number>]`

`[body]`

Elements inside `[]` are optional

#### `type`

The type of the modification you are commiting.

For example, if you are adding a new functionality it will be `feat`.

If you are solving a bug it will be `fix`.

Here is a list of `type` keyword:

- `feat`: adding of a new functionality / new library
- `fix`: Resolving a problem (bug, wrong display)
- `doc`: Adding new documentation
- `refact`: Refacting a part of code / all the code
- `test`: Adding tests
- `improvment`: Upgrading an existing functionality

#### `scope`

The scope is optionnal, it's an indication of the part of code / functionality your commiting into.

For exemple, if you're adding a ci you can use: `feat(ci)`.

#### `description`

The description is a brief text that describe what you are committing.

#### `issue-number`

The issue number allows to link your commit to an issue.

It's also on what part of the code your working.

#### `body`

The body is an optionnal additionnal information to add supports (links, images, ...).

Or just describe in details the problem you are fixing.

#### Examples

##### Commiting a new language on an application

`feat(language): Adding of the russian language #14`.

`Adding the russian option for the translation of the home page`.

##### Commiting a bugfix

`fix(keyboard): Removing the blinking effect when pressing keys #54`.

##### Commiting new tests

`test(authentication): Adding full test on the authentication`.

To help you on commit convention, you can execute the `./devToolsSetup.sh` script.  
This will install a script that automatically check your commits.

### Merge requests convention

When creating a merge request, it creates a commit.  
This commit must look like the following:  
`merge(<source_branch>): into <target_branch> #<issue_number>`  

Where `source_branch` is the branch we merge from and `target_branch` is the branch we merge on.  
`issue_number` is the issue id that the merge request should close.  

#### Example

Merging the branch `13_dev_Create-clock` into `dev` and close the #13 issue.

```
merge(13_dev_Create-clock): into dev #13
